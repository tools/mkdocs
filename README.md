# mkdocs


TL:DR:
- manually bump version in `version/version.py`
- CI will push to the registry using the latest version
 
This builds [mkdocs] into a Docker image to be used in CI jobs. For example:

    mkdocs:
      stage: build
      image: registry.gitlab.vgiscience.org/tools/mkdocs:latest
      script:
        - mkdocs build
    artifacts:
      name: pages
      paths:
        - site

[mkdocs]: https://www.mkdocs.org/
