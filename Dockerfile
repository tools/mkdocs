FROM python:3

# add pandoc
ENV PANDOC_VERSION 3.5
ENV PANDOC_DOWNLOAD_URL https://github.com/jgm/pandoc/releases/download/$PANDOC_VERSION/pandoc-$PANDOC_VERSION-1-amd64.deb

RUN curl -fsSL "$PANDOC_DOWNLOAD_URL" -o pandoc.deb \
 && dpkg -i pandoc.deb

# add mkdocs
RUN pip install --no-cache-dir mkdocs && \
    pip install --no-cache-dir mkdocs-material && \
    pip install --no-cache-dir mkdocs-material-extensions && \
    pip install --no-cache-dir markdown-include && \
    pip install --no-cache-dir pygments && \
    pip install --no-cache-dir pymdown-extensions && \
    pip install --no-cache-dir mkdocs-minify-plugin && \
    pip install --no-cache-dir mkdocs-git-revision-date-localized-plugin && \
    pip install --no-cache-dir mkdocs-bibtex && \
    pip install --no-cache-dir mkdocs-mermaid2-plugin && \
    pip install --no-cache-dir mkdocs-git-authors-plugin && \
    pip install --no-cache-dir mkdocs-table-reader-plugin && \
    pip install --no-cache-dir mkdocs-include-markdown-plugin
